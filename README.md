<div align="center">
   <img src="src/Vista/icons/image_esc.png" width=500px >
</div>

# Título del proyecto:

#### Software para manejo de inventario y facturación de la droguería Prontitud.
***
## Índice
1. [Características](#características) ⚙ 
2. [Contenido del proyecto](#contenido-del-proyecto) 📎 
3. [Tecnologías](#tecnologías) 🖥 
4. [IDE](#ide) 🖱💻 
5. [Instalación](#instalación) 📥 
6. [Demo](#demo) ⌚ 
7. [Autor(es)](#autores) 🧑‍💻
8. [Institución Académica](#institución-académica) 🏫 
9. [Referencias](#referencias) 📓 
***

#### Características
- Proyecto con arquitectura MVC (modelo, vista y controlador)  

<div align="center">
   <img src="http://www.juntadeandalucia.es/servicios/madeja/sites/default/files/imagecache/wysiwyg_imageupload_big/wysiwyg_imageupload/10/MVC_0.png" width=200px >
</div>


- Implementación de Java Swing para interfaz gráfica de usuario 
- Herramienta de mapeo ORM  : ![**Hibernate**  versión: ](<https://www.edureka.co/blog/what-is-hibernate-in-java/>) [![4.3+](https://img.shields.io/badge/-4.3+-lightgrey)](#)

<div align="center">
   <img src="https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2019/08/2019-08-01-17_35_16-Window.png" width=400px >
</div>

- Conversión de los objetos Java en instrucciones para el manejador de la base de datos por medio del modelo de persistencia ![**JPA**  versión : ](<https://www.oscarblancarteblog.com/tutoriales/java-persistence-api-jpa/>) [![2.1](https://img.shields.io/badge/-2.1-lightgrey)](#) Java Persistence API 

***

#### Contenido del proyecto

|  Archivo 	|  Descripción 	|
|---	|---	|
|  ![Exceptions](<https://gitlab.com/GenesisDaniela/sistema-inventario-drogueria-prontitud/-/tree/master/src/Controladores/exceptions>) 	|  Manejo de excepciones 	|
|  ![Controladores](<https://gitlab.com/GenesisDaniela/sistema-inventario-drogueria-prontitud/-/tree/master/src/Controladores>) 	|    Permite interactuar con la base de datos por medio de objetos, de esta forma, JPA es el encargado de convertir los objetos Java en instrucciones para el Manejador de Base de Datos (MDB)	|
| ![persistence.xml](<src/META-INF/persistence.xml>) | Define todos los metadatos necesarios para iniciar una EntityManagerFactory, como asignaciones de entidades, fuente de datos y configuración de transacciones, así como propiedades de configuración del proveedor JPA |
| ![Conexion.java](<src/Modelo/Conexion.java>) | Permite conectar nuestra aplicación con la base de datos |
| ![Modelo](<https://gitlab.com/GenesisDaniela/sistema-inventario-drogueria-prontitud/-/tree/master/src/Modelo>) | Contiene una representación de los datos que maneja el sistema, su lógica de negocio, y sus mecanismos de persistencia| 
| ![Vista](<https://gitlab.com/GenesisDaniela/sistema-inventario-drogueria-prontitud/-/tree/master/src/Vista>)  |  También conocida como interfaz de usuario, la cuál compone la información que se envía al cliente y los mecanismos interacción con éste. |

***

#### Tecnologías
| Tecnología | Descripción |
|---	|---	|
| [![Java](https://img.shields.io/badge/-Java-red)](https://www.java.com/es/about/whatis_java.jsp) | Java es una tecnología que se usa para el desarrollo de aplicaciones que convierten a la Web en un elemento más interesante y útil. Java no es lo mismo que javascript, que se trata de una tecnología sencilla que se usa para crear páginas web y solamente se ejecuta en el explorador. |
| [![Java Swing](https://img.shields.io/badge/-Java%20Swing-blue)](https://docs.oracle.com/javase/7/docs/api/javax/swing/package-summary.html) | Proporciona un conjunto de componentes "ligeros" (totalmente en lenguaje Java) que, en la medida de lo posible, funcionan igual en todas las plataformas. Swing es una biblioteca gráfica para Java. Incluye widgets para interfaz gráfica de usuario tales como cajas de texto, botones, listas desplegables y tablas.|
| [![Hibernate](https://img.shields.io/badge/-Hibernate-green)](https://www.edureka.co/blog/what-is-hibernate-in-java/) | Hibernate es un marco en Java que viene con una capa de abstracción y maneja las implementaciones internamente. Las implementaciones incluyen tareas como escribir una consulta para CRUD operaciones o establecer una conexión con las bases de datos, etc. Un Framework es básicamente un software que proporciona abstracción en múltiples tecnologías como JDBC , servlet, etc.  | 
| [![JPA](https://img.shields.io/badge/-JPA-yellow)](https://vladmihalcea.com/jpa-persistence-xml/) |  La unidad de persistencia define todos los metadatos necesarios para iniciar un EntityManagerFactory, como asignaciones de entidades, fuente de datos y configuración de transacciones, así como propiedades de configuración del proveedor de JPA. El objetivo de la EntityManagerFactory se usa para crear EntityManagerobjetos que podamos para las transiciones de estado de entidad. |

***

#### IDE

- El proyecto se desarrolló en **Netbeans** , es un entorno de desarrollo gratuito y de código abierto. Permite el uso de un amplio rango de tecnologías de desarrollo tanto para escritorio, como aplicaciones Web, o para dispositivos móviles. Da soporte a las siguientes tecnologías, entre otras: Java, PHP, Groovy, C/C++, HTML5,... Además puede instalarse en varios sistemas operativos: Windows, Linux, Mac OS...

-![   Calendamaia](<https://www.genbeta.com/autor/calendamaia>)

<div align="center">
   <img src="https://www.mancomun.gal/wp-content/uploads/2019/07/apache-netbeans.png" width=400px heigth=200px>
</div>

Para más información: [Ver.     ](<https://www.genbeta.com/autor/calendamaia>) _Articulo "Netbeans IDE"_ 

***

#### Instalación
 
Descargar Java e instalar Java con Windows de forma manual 
- Para descargar Java para cualquier sistema operativo acceda [Aquí.](<https://www.java.com/es/download/manual.jsp>)
- Haga clic en **Windows en línea**.
- Aparecerá el cuadro de diálogo Descarga de archivos y le pedirá que ejecute o guarde el archivo descargado
- Para ejecutar el instalador, haga clic en **Ejecutar**.
- Para guardar el archivo y ejecutarlo más tarde, haga clic en **Guardar**.
- Seleccione la ubicación de la carpeta y guarde el archivo en el sistema local
- Sugerencia: Guarde el archivo en una ubicación conocida de su equipo; por ejemplo, en el escritorio
- Haga **doble clic** en el archivo guardado para iniciar el proceso de instalación.
- Se iniciará el proceso de instalación. Haga clic en el botón **Instalar** para aceptar los términos de la licencia y continuar con la instalación.  

<div align="center">
   <img src="https://www.java.com/content/published/api/v1.1/assets/CONT4E269030F8534030887506B2017A4F38/native?cb=_cache_1918&channelToken=1f7d2611846d4457b213dfc9048724dc" width=400px heigth=200px>
</div>

- Oracle colabora con empresas que ofrecen distintos productos. Es posible que el instalador le ofrezca la opción de instalar estos programas como parte de la instalación de Java. Una vez seleccionados los programas que desee, haga clic en el botón **Siguiente** para proseguir con el proceso de instalación.
- Se abrirán varios cuadros de diálogo con información para completar las últimas etapas del proceso de instalación; haga clic en **Cerrar** en el último cuadro de diálogo. Con esta acción se completará el proceso de instalación de Java.

- ℹ️ **Detectar versiones anteriores (8u20 y versiones posteriores)**. A partir de Java 8 Update 20 (8u20), en los sistemas Windows, la herramienta de desinstalación de Java está integrada con el installer para contar con una opción para eliminar las versiones anteriores de Java del sistema. El cambio se aplica a plataformas Windows de 32 bits y 64 bits.

- **Notificaciones sobre Java desactivado y restauración de peticiones de datos**
Installer le notifica si el contenido de Java está desactivado en los exploradores web y proporciona instrucciones para activarlo. Si había elegido ocultar algunas peticiones de datos de seguridad para applets y aplicaciones de Java Web Start, Installer ofrece una opción para restaurar las peticiones de datos. Puede que el instalador le pida reiniciar la computadora si, cuando se le solicitó, optó por no reiniciar el explorador de Internet 


[_Para más información: Manual de instalación._](<https://drive.google.com/file/d/1vltLkPbbGi502GW_OXPS9bQDMgMDteij/view?usp=sharing>)

***

#### Demo
Programa en ejecución 

- Como administrador: 

   1. Inicio de sesión  

   <div align="center">
      <img src="src/Vista/Img_demo/paso1.png" width=400px heigth=200px>
   </div> 

   2. Se presentan las opciones a las cuáles tiene acceso permitido  
   <div align="center">
      <img src="src/Vista/Img_demo/paso3.png" width=400px heigth=200px>
   </div> 

   3. Podemos registrar o agregar un producto, donde todos nuestros datos son de carácter obligatorio  
   <div align="center">
      <img src="src/Vista/Img_demo/paso_6.png" width=400px heigth=200px>
   </div> 

   4. Se presenta la tabla de clientes, donde podemos filtrar (por medio de la identificación) modificar y eliminar  un cliente  
   <div align="center">
      <img src="src/Vista/Img_demo/paso11.png" width=400px heigth=200px>
   </div> 


- Como Vendedor:

   1. Inicio de sesión  

   <div align="center">
      <img src="src/Vista/Img_demo/pasov1.png" width=400px heigth=200px>
   </div> 

   2. Se presentan las opciones a las cuáles el vendedor tiene acceso permitido al sistema, donde tendremos algunas limitaciones.  
   <div align="center">
      <img src="src/Vista/Img_demo/pasov2.png" width=400px heigth=200px>
   </div>

   3. Se muestra la tabla de productos, donde podemos filtrar pero no podemos eliminar ni modificar el producto seleccionado  
   <div align="center">
      <img src="src/Vista/Img_demo/pasov3.png" width=400px heigth=200px>
   </div> 

   4. Se muestra la tabla de lote, donde podemos filtrar, eliminar y modificar el lote seleccionado    
   <div align="center">
      <img src="src/Vista/Img_demo/pasov4.png" width=400px heigth=200px>
   </div> 


[_Para más información: Manual de usuario_](<https://drive.google.com/file/d/1isQ8Ls5w5MF1WGRw8JZmhxkU6yOegTg_/view?usp=sharing>)

***

#### Autor(es)
Proyecto desarrollado por 
- [Génesis Daniela Vargas Jáuregui](<https://gitlab.com/GenesisDaniela>) (<genesisdanielavjau@ufps.edu.co>)
- [Paula Valentina Rico Lindarte](<https://gitlab.com/paulavalentinarlin>) (<paulavalentinarlin@ufps.edu.co>)
- [Edinsson Adrián Melo Calvo](<https://github.com/Mackenzie-98>) (<edinssonadrianmc@ufps.edu.co>)

***

#### Institución Académica
Proyecto desarrollado en la Materia Bases de datos del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]

   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>
  
***

#### Referencias

- Java Persistence API (JPA). (2020, 25 junio). Oscar Blancarte - Software Architecture. https://www.oscarblancarteblog.com/tutoriales/java-persistence-api-jpa/
- Modelo vista controlador (MVC). Servicio de Informática ASP.NET MVC 3 Framework. (s. f.). Universidad de Alicante. https://si.ua.es/es/documentacion/asp-net-mvc-3/1-dia/modelo-vista-controlador-mvc.html
- V. (2020, 30 abril). A beginner’s guide to JPA persistence.xml file. Vlad Mihalcea. https://vladmihalcea.com/jpa-persistence-xml/
- Waseem, M. (2019, 27 noviembre). What is Hibernate in Java and Why do we need it? Edureka. https://www.edureka.co/blog/what-is-hibernate-in-java/
***


